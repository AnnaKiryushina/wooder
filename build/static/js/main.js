window.onscroll = function() {

    //Header changes its color wilst scrolling
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    $('header').css('background', (scrolled == 0) ? "transparent" : "black");
}

$(document).ready(function () {

    $('.header__menu-icon__wrap').mouseover(function () {
        $('.header__menu-icon__col').css('background', '#bea67c');
        $('.all-menu-items').css('display', 'flex');
        $('.header__menu-icon').attr('src', 'static/img/menu-of-three-lines-on.svg');
    });

    $('.all-menu-items').mouseleave(function () {
        $('.header__menu-icon__col').css('background', 'none');
        $('.all-menu-items').css('display', 'none');
        $('.header__menu-icon').attr('src', 'static/img/menu-of-three-lines.svg');
    });
    //title-banner length
    var tb_len = 300 + $('.title-banner-content').height() + 210;
    $('.title-banner').css('height', tb_len);
    $('.img-wrap1').css('height', tb_len - 40);
    $('.img-wrap2').css('height', tb_len);

    //furniture and decor text position
    $('.right-text').css('right', -$('.right-text').width());

    //Select Language

    var langList = $('.lang-select');
    var opts = ["EN", "RU", "FR"];
    var optsLen = opts.length;
    var strOpt = '';

    for (var i = 0; i < optsLen; i++) {
        strOpt = "<option class='opt' value='" + i + "'>" + opts[i] + "</option>";
        $(langList).append(strOpt);
    }

    $('.opt').css({
        'height': '1em',
        'background-color': 'transparent',
        'border': '0'
    });

    $('.title-slider').slick({
        centerMode: false,
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 6000,
        dots: true,
        arrows: false
    });

    $('.slick-dots').find('li').find('button').html('');


    //furniture-and-decor  background signs positions
    var pos1 = $('.odd').offset();
    var pos2 = $('.even').offset();

    $('.furniture').css({
        'top': pos1.top + $('.odd').height / 2,
        'left': $('.odd').width * 0.6
    });

    $('.decor').css({
        'left' : '20px',
        'top' : pos2.top + $('.even').height / 2
    });

    $('.round-border').css('height', $('.round-border').width());

    $('.video-window').css('height', $('.video-window').width());

    $('.play-button__wrap').css('height', $('.play-button__wrap').width());

    $('.about-us__img').css('height', $('.about-us__img').width() * 0.695);
});